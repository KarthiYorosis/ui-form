import { Component, OnInit } from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private fb: FormBuilder) { }

  title = 'ui-form';
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  fruits: Fruit[] = [
    {name: 'Lemon'},
    {name: 'Lime'},
    {name: 'Apple'},
  ];

  form: FormGroup;
  ngOnInit() {
    this.form = this.fb.group({
      services: this.fb.array([
        this.addService()
      ])
    });
  }

  addService(): FormGroup {
    return this.fb.group({
     fromDate: ['', Validators.required],
     toDate: ['', Validators.required],
     procedure: ['', Validators.required],
     revenue: ['', Validators.required],
     npi: ['', Validators.required],
     taxonomy: ['', Validators.required],
     charge: ['', Validators.required],
     chargeUnits: ['', Validators.required],
     diagnosisOptions: ['', Validators.required],
     secondrydiagnosis: ['', Validators.required],
     paidAmount: ['', Validators.required],
     allowedAmount: ['', Validators.required],
     padiUnits: ['', Validators.required],
     claimType: ['', Validators.required],
     errorCodes: ['', Validators.required],
   });
  }


  addAnotherService(i): void {
    console.log(i);
    (this.form.get('services') as FormArray).reset(i);
    (<FormArray>this.form.get('services')).push(this.addService());
  }

  removeThisService(i) {
    (this.form.get('services') as FormArray).removeAt(i);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.fruits.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(fruit: Fruit): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }
}
export interface Fruit {
  name: string;
}
